USE ModernWays;
ALTER TABLE Liedjes ADD COLUMN Genre VARCHAR(20);
SET SQL_SAFE_UPDATES = 0;
UPDATE Liedjes SET Genre = 'Hard Rock' WHERE Artiest = 'Led Zeppelin' or Artiest = 'Van Halen';
SET SQL_SAFE_UPDATES =1;