DROP DATABASE IF EXISTS ModernWays;
CREATE DATABASE ModernWays;
USE ModernWays;

create table Metingen(
Tijdstip char(100) char set utf8mb4,
Grootte varchar (300) char set utf8mb4,
Marge char (50) char set utf8mb4);