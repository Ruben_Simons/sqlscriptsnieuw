USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
update huisdieren
set leeftijd = 9
WHERE soort = 'hond' AND baasje = 'Christiane'
OR soort = 'kat' AND baasje = 'Bert';
SET SQL_SAFE_UPDATES = 1;